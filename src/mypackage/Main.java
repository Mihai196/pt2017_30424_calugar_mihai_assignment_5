package mypackage;

import java.io.IOException;

public class Main {
	public static void main(String[] args) throws IOException {
		MonitoredData activityObj = new MonitoredData();
		activityObj.readfromFile();
		activityObj.countDistinctDays();
		activityObj.distAction();
		activityObj.activityCountperDay();
		activityObj.totDuration();
	}

}

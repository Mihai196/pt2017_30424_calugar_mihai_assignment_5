package mypackage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class MonitoredData {

	private DateTime startTime;
	private DateTime endTime;
	private String activityLabel;

	public MonitoredData() {

	}

	public DateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(DateTime startTime) {
		this.startTime = startTime;
	}

	public DateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(DateTime endTime) {
		this.endTime = endTime;
	}

	public String getActivityLabel() {
		return activityLabel;
	}

	public void setActivity(String activity) {
		this.activityLabel = activity;
	}

	Function<String, MonitoredData> mapFromFiletoMonitData = new Function<String, MonitoredData>() {

		public MonitoredData apply(String linefromFile) {

			MonitoredData activityObj = new MonitoredData();
			DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
			String[] detailactivity = linefromFile.split("\\t+");

			try {
				activityObj.startTime = formatter.parseDateTime(detailactivity[0]);
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				activityObj.endTime = formatter.parseDateTime(detailactivity[1]);
			} catch (Exception e) {
				e.printStackTrace();
			}
			activityObj.activityLabel = detailactivity[2];

			return activityObj;
		}
	};
	List<MonitoredData> monitDataList = new ArrayList<MonitoredData>();
	public void readfromFile() {

		try {
			String FILENAME = "Activities.txt";
			BufferedReader readbuff = new BufferedReader(new FileReader(FILENAME));
			monitDataList = readbuff.lines().map(mapFromFiletoMonitData).collect(Collectors.toList());
			readbuff.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void countDistinctDays() {

		String FILENAME = "countdays.txt";
		BufferedWriter bw = null;
		FileWriter fw = null;
		try {
			int x = monitDataList.parallelStream().collect(Collectors.collectingAndThen(
					Collectors.groupingBy(activity -> activity.getStartTime().dayOfMonth(), Collectors.counting()),
					Map::size));
			String detail = "The number of distinct days in the log is ";
			String towrite = Integer.toString(x);
			fw = new FileWriter(FILENAME);
			bw = new BufferedWriter(fw);
			bw.write(detail);
			bw.write(towrite);

			System.out.println("Task 1: Counting distinct days in log performed successfully (results in text file)");

		} catch (IOException e) {

			e.printStackTrace();
		} finally {

			try {

				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();

			} catch (IOException ex) {
				ex.printStackTrace();

			}
		}
	}

	public void distAction() {
		String FILENAME = "distActionOccur.txt";
		BufferedWriter bw = null;
		FileWriter fw = null;
		try {

			String eachAction = monitDataList.parallelStream()
					.collect(Collectors.groupingBy(activity -> activity.getActivityLabel(), Collectors.counting()))
					.toString();
			fw = new FileWriter(FILENAME);
			bw = new BufferedWriter(fw);
			bw.write(eachAction);
			System.out.println("Task 2: Each distinct activity was identified (results in text file)");

		} catch (IOException e) {

			e.printStackTrace();
		} finally {

			try {

				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();

			} catch (IOException ex) {
				ex.printStackTrace();

			}
		}

	}

	public void activityCountperDay() {
		String FILENAME = "activityCountperDay.txt";
		BufferedWriter bw = null;
		FileWriter fw = null;
		try {

			String activperDay = monitDataList.parallelStream()
					.collect(Collectors.groupingBy(activity -> activity.getStartTime().dayOfMonth().getAsShortText(),
							Collectors.groupingBy(activitycrit -> activitycrit.getActivityLabel(), Collectors.counting())))
					.toString();
			fw = new FileWriter(FILENAME);
			bw = new BufferedWriter(fw);
			bw.write(activperDay);
			System.out.println(
					"Task 3: Each distinct activity was identified and the occurences in each day (results in text file)");

		} catch (IOException e) {

			e.printStackTrace();
		} finally {

			try {

				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();

			} catch (IOException ex) {
				ex.printStackTrace();

			}
		}
	}

	public void totDuration() {
		String FILENAME = "totDuration.txt";
		BufferedWriter bw = null;
		FileWriter fw = null;
		try {
			Map<Object, Double> maptotDuration = monitDataList.parallelStream()
					.collect(Collectors.groupingBy(activity -> activity.getActivityLabel(),
							Collectors.summingDouble(monitoredData -> (monitoredData.getEndTime().getMillis()
									- monitoredData.getStartTime().getMillis()) / 3600000)));
			String filtered = maptotDuration.entrySet().parallelStream().filter(entry -> entry.getValue() > 10)
					.collect(Collectors.toList()).toString();
			fw = new FileWriter(FILENAME);
			bw = new BufferedWriter(fw);
			bw.write(filtered);

			System.out.println(
					"Task 4: Each activity with total duration was identified and filtered(>10 hours) (results in text file)");

		} catch (IOException e) {

			e.printStackTrace();
		} finally {

			try {

				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();

			} catch (IOException ex) {
				ex.printStackTrace();

			}
		}
	}

}
